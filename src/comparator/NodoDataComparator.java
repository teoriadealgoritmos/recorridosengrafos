package comparator;

import java.util.Comparator;

import model.NodoData;

public class NodoDataComparator implements Comparator<NodoData> {
    public int compare(NodoData nodoAEstrella1, NodoData nodoAEstrella2) {
        if (nodoAEstrella1.getPesoTotal() > nodoAEstrella2.getPesoTotal()) return 1;
        else if (nodoAEstrella2.getPesoTotal() > nodoAEstrella1.getPesoTotal()) return -1;
        return 0;
    }
} 