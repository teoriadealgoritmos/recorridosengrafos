package heuristicas;

public interface Heuristicable {
	
	public int heuristica(int vertice);
}
