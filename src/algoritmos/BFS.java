package algoritmos;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import model.Arista;
import model.Grafo;

public class BFS extends Camino {

	private double distancias[];
	private Arista aristas[];

	public BFS(Grafo grafo, int origen, int destino) {
		super(grafo, origen);
		this.distancias = new double[grafo.getCANT_VERTICES()];
		this.aristas = new Arista[grafo.getCantidadAristas()];
		Arrays.fill(distancias, Double.POSITIVE_INFINITY);
		
		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(origen);
		int dist = 0;
		distancias[origen] = dist;
		
		while (!queue.isEmpty() && queue.peek() != destino) {
			int v = queue.poll();
			dist++;
			for (Arista a : grafo.adj(v)) {
				int w = a.getDestino();
				if ( !super.visitado(w) ) {
					aristas[w] = a;
					distancias[w] = dist;
					queue.add(w);
				}
			}
		}
	}

	@Override
	public double distancia(int v) {
		return this.distancias[v];
	}

	@Override
	protected Arista aristaHasta(int v) {
		return this.aristas[v];
	}

}
