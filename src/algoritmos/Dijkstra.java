package algoritmos;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

import model.Arista;
import model.Grafo;
import model.NodoData;

import comparator.NodoDataComparator;

public class Dijkstra extends Camino {

	private double distancias[];
	private Arista aristas[];

	public Dijkstra(Grafo grafo, int origen, int destino) {
		super(grafo, origen);

		aristas = new Arista[grafo.getCantidadAristas()];
		distancias = new double[grafo.getCANT_VERTICES()];
		Arrays.fill(distancias, Double.POSITIVE_INFINITY);

		// Partial Distance
		double[] D = distancias.clone();
		D[origen] = 0;

		Queue<NodoData> heapAbierto = new PriorityQueue<NodoData>(
				grafo.getCANT_VERTICES(), new NodoDataComparator());

		NodoData nodoVisitado = new NodoData(origen, 0);

		while (!visitado(destino)) {
			distancias[nodoVisitado.getVertice()] = nodoVisitado.getPesoTotal();
			Iterable<Arista> aristasAdy = grafo.adj(nodoVisitado.getVertice());
			for (Arista arista : aristasAdy) {
				NodoData nodoAdy = new NodoData(arista.getDestino(),
						arista.getPeso());
				if (!visitado(nodoAdy.getVertice())) {
					if (D[arista.getOrigen()] + arista.getPeso() < D[nodoAdy
							.getVertice()]) {
						nodoAdy.setPesoTotal(D[arista.getOrigen()]
								+ arista.getPeso());
						D[nodoAdy.getVertice()] = nodoAdy.getPesoTotal();
						aristas[nodoAdy.getVertice()] = arista;
						heapAbierto.add(nodoAdy);
					}
				}
			}

			nodoVisitado = heapAbierto.poll();

		}
	}

	@Override
	public double distancia(int v) {
		return this.distancias[v];
	}

	@Override
	protected Arista aristaHasta(int v) {
		return this.aristas[v];
	}

}
