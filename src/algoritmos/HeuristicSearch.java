package algoritmos;

import java.util.Arrays;
import java.util.PriorityQueue;

import comparator.NodoDataComparator;
import heuristicas.Heuristicable;
import model.Arista;
import model.Grafo;
import model.NodoData;

public class HeuristicSearch extends Camino {
	
	private double distancias[];
	private Arista aristas[];
	
	public HeuristicSearch(Grafo grafo, int origen, int destino, Heuristicable h) {
		super(grafo, origen);
		this.distancias = new double[grafo.getCANT_VERTICES()];
		this.aristas = new Arista[grafo.getCantidadAristas()];
		Arrays.fill(distancias, Double.POSITIVE_INFINITY);
		
		PriorityQueue<NodoData> queue = new PriorityQueue<NodoData>(
				grafo.getCANT_VERTICES(), new NodoDataComparator());
		int dist = 0;
		NodoData nodo = new NodoData(origen, h.heuristica(origen));
		queue.add(nodo);
		distancias[origen] = dist;

		while (!queue.isEmpty() && queue.peek().getVertice() != destino) {
			int v = queue.poll().getVertice();
			dist++;
			for (Arista a : grafo.adj(v)) {
				int w = a.getDestino();
				if ( !super.visitado(w) ) {
					aristas[w] = a;
					distancias[w] = dist;
					
					queue.add(new NodoData(w, h.heuristica(w)));
				}
			}
		}
	}

	@Override
	public double distancia(int v) {
		return this.distancias[v];
	}

	@Override
	protected Arista aristaHasta(int v) {
		return this.aristas[v];
	}
}
