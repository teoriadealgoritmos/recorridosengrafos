package algoritmos;

import heuristicas.Heuristicable;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

import model.Arista;
import model.Grafo;
import model.NodoData;

import comparator.NodoDataComparator;

public class AEstrella extends Camino {

	private double distancias[];
	private Arista aristas[];

	public AEstrella(Grafo grafo, int origen, int destino, Heuristicable h) {
		super(grafo, origen);
		this.distancias = new double[grafo.getCANT_VERTICES()];
		this.aristas = new Arista[grafo.getCantidadAristas()];
		Arrays.fill(distancias, Double.POSITIVE_INFINITY);

		final Queue<NodoData> heapAbierto = new PriorityQueue<NodoData>(11,
				new NodoDataComparator());

		int dist = 0;
		distancias[origen] = dist;
		heapAbierto.add(new NodoData(origen, h.heuristica(origen)));

		while (!heapAbierto.isEmpty()) {
			final NodoData nodoAEstrella = heapAbierto.poll();

			if (nodoAEstrella.getVertice() != destino) {
				for (Arista a : grafo.adj(nodoAEstrella.getVertice())) {
					int w = a.getDestino();
					if (!super.visitado(w)) {
						double tentativeG = a.getPeso()
								+ distancias[nodoAEstrella.getVertice()];

						if (tentativeG < w
								|| !heapAbierto.contains(new NodoData(w, h
										.heuristica(w)))) {
							distancias[w] = tentativeG;
							aristas[w] = a;
							heapAbierto.add(new NodoData(w, h.heuristica(w)));
						}
					}
				}
			} else
				break;
		}

	}

	@Override
	public double distancia(int v) {
		return this.distancias[v];
	}

	@Override
	protected Arista aristaHasta(int v) {
		return this.aristas[v];
	}

}
