package algoritmos;

import java.util.List;
import java.util.Stack;

import model.Arista;
import model.Grafo;

public abstract class Camino {

	private int origen;

	protected Camino(Grafo grafo, int origen) {
		this.origen = origen;
	}

	public abstract double distancia(int v);

	protected abstract Arista aristaHasta(int v);

	public boolean visitado(int vertice) {
		return distancia(vertice) < Double.POSITIVE_INFINITY;
	}

	public List<Arista> camino(int vertice) {
		Stack<Arista> camino = null;
		if (visitado(vertice)) {
			camino = new Stack<Arista>();
			while (vertice != this.origen) {
				Arista a = aristaHasta(vertice);
				camino.push(a);
				vertice = a.getOrigen();
			}
		}
		return camino;
	}
}