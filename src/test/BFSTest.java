package test;

import java.util.List;

import model.Arista;
import model.Grafo;

import org.junit.Test;

import algoritmos.BFS;

public class BFSTest {

	@Test
	public void test() {
		Grafo grafo = new Grafo(6);
		Arista arista1 = new Arista(0, 1, 2);
		Arista arista2 = new Arista(1, 2, 2);
		Arista arista3 = new Arista(1, 3, 2);
		Arista arista4 = new Arista(1, 4, 2);
		Arista arista5 = new Arista(2, 3, 2);
		Arista arista6 = new Arista(3, 4, 2);
		Arista arista7 = new Arista(4, 5, 2);
		grafo.agregarArista(arista1);
		grafo.agregarArista(arista2);
		grafo.agregarArista(arista3);
		grafo.agregarArista(arista4);
		grafo.agregarArista(arista5);
		grafo.agregarArista(arista6);
		grafo.agregarArista(arista7);
		
		BFS bfs = new BFS(grafo, 0, 5);
		List<Arista> st = bfs.camino(5);
		for (Arista arista : st) {
			System.out.println("Arista: origen: "+ arista.getOrigen() + " Destino: " + arista.getDestino());
		}
		
	}

}
