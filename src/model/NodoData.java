package model;

public class NodoData {
	private int vertice;
	private double pesoTotal;
	
	public NodoData(int vertice, double pesoTotal) {
		this.vertice = vertice;
		this.pesoTotal = pesoTotal;
	}
	
	public int getVertice() {
		return vertice;
	}
	public void setVertice(int vertice) {
		this.vertice = vertice;
	}
	public double getPesoTotal() {
		return pesoTotal;
	}
	public void setPesoTotal(double pesoTotal) {
		this.pesoTotal = pesoTotal;
	}
	
	
}
