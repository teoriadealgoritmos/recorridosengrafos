package model;

public class Grafo {
	private final int CANT_VERTICES;
	private int cantidadAristas;

	private Bag<Arista>[] aristas;

	@SuppressWarnings("unchecked")
	public Grafo(int cantidadVertices) {
		CANT_VERTICES = cantidadVertices;
		this.cantidadAristas = 0;
		this.aristas = (Bag<Arista>[]) new Bag[cantidadVertices];
		for (int i = 0; i < CANT_VERTICES; i++) {
			this.aristas[i] = new Bag<Arista>();
		}
	}

	public void agregarArista(Arista arista) {
		this.aristas[arista.getOrigen()].add(arista);
		this.cantidadAristas++;
	}

	public Iterable<Arista> adj(int vertice) {
		return this.aristas[vertice];
	}

	public Iterable<Arista> aristas() {
		Bag<Arista> aristas = new Bag<Arista>();
		for (int i = 0; i < CANT_VERTICES; i++)
			for (Arista arista : this.aristas[i])
				aristas.add(arista);
		return aristas;
	}

	public int getCANT_VERTICES() {
		return CANT_VERTICES;
	}

	public int getCantidadAristas() {
		return cantidadAristas;
	}

}
